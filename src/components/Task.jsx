import { Button, Container, Grid, List, ListItem, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { addTaskClickAction, inputTaskNameAction, toggleTaskAction } from "../actions/task.actions";

const Task = () => {
    // Khai báo hàm dispatch sự kiện tới redux store
    const dispatch = useDispatch();

    // useSelector để đọc state từ redux
    const { inputString, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer;
    })

    const inputChangeHandler = (event) => {
        dispatch(inputTaskNameAction(event.target.value));
    }

    const addTaskClickHandler = () => {
        dispatch(addTaskClickAction());
    }

    const onClickToggleItem = (index) => {
        dispatch(toggleTaskAction(index));
    }

    return (
        <Container>
            {/* Là 1 hàng */}
            <Grid container spacing={2} mt={5} alignItems="center">
                {/* Là 1 cột */}
                <Grid item md={9}>
                    <TextField label="Task name" variant="outlined" placeholder="Input task here" fullWidth value={inputString} onChange={inputChangeHandler}/>
                </Grid>
                <Grid item md={3}>
                    <Button variant="contained" onClick={addTaskClickHandler}>Add task</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List dense={false}>
                    {
                        taskList.map((element, index) => {
                            return (
                                <ListItem key={index} style={{color: element.status ? "green" : "red"}} onClick={() => onClickToggleItem(index)}>
                                    {index+1}. {element.taskName}
                                </ListItem>
                            )
                        })
                    }
                </List>
            </Grid>
        </Container>
    )
}

export default Task;