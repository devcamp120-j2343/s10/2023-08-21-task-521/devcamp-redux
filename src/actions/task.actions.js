import { ADD_TASK_CLICKED, INPUT_TASK_NAME, TOGGLE_STATUS_TASK } from "../constants/task.constants"

export const inputTaskNameAction = (inputValue) => {
    return {
        type: INPUT_TASK_NAME,
        payload: inputValue
    }
}

export const addTaskClickAction = () => {
    return {
        type: ADD_TASK_CLICKED
    }
}

export const toggleTaskAction = (taskIndex) => {
    return {
        type: TOGGLE_STATUS_TASK,
        payload: taskIndex
    }
}